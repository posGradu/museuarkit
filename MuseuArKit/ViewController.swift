//
//  ViewController.swift
//  MuseuArKit
//
//  Created by aluno on 18/04/19.
//  Copyright © 2019 projeto. All rights reserved.
//
import UIKit
import SceneKit
import ARKit

class ViewController: UIViewController, ARSCNViewDelegate {

    @IBOutlet var sceneView: ARSCNView!
    
    var isQuadro1SubtitleDisplayed = false;
    var isQuadro2SubtitleDisplayed = false;
    var isQuadro3SubtitleDisplayed = false;
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Set the view's delegate
        sceneView.delegate = self
        
        // Show statistics such as fps and timing information
        sceneView.showsStatistics = true
        
        //        let scene = SCNScene(named: "art.scnassets/ship.scn")!
        //        sceneView.scene = scene
        
        setupScene()
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        // Create a session configuration
        let configuration = ARWorldTrackingConfiguration()
        // Run the view's session
        sceneView.session.run(configuration)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        // Pause the view's session
        sceneView.session.pause()
    }
    
    func setupScene() {
        let node = SCNNode()
        node.position = SCNVector3.init(0, 0, 0)
        
        let leftWall = createBox(isDoor: false)
        leftWall.position = SCNVector3.init((-length / 2) + width, 0, 0)
        leftWall.eulerAngles = SCNVector3.init(0, 180.0.degreesToRadians, 0)
        
        let rightWall = createBox(isDoor: false)
        rightWall.position = SCNVector3.init((length / 2) - width, 0, 0)
        
        let topWall = createBox(isDoor: false)
        topWall.position = SCNVector3.init(0, (height / 2) - width, 0)
        topWall.eulerAngles = SCNVector3.init(0, 0, 90.0.degreesToRadians)
        
        let bottomWall = createBox(isDoor: false)
        bottomWall.position = SCNVector3.init(0, (-height / 2) + width, 0)
        bottomWall.eulerAngles = SCNVector3.init(0, 0, -90.0.degreesToRadians)
        
        let backWall = createBox(isDoor: false)
        backWall.position = SCNVector3.init(0, 0, (-length / 2) + width)
        backWall.eulerAngles = SCNVector3.init(0, 90.0.degreesToRadians, 0)
        
        let leftDoorSide = createBox(isDoor: true)
        leftDoorSide.position = SCNVector3.init((-length / 2 + width) + (doorLength / 2), 0, (length / 2) - width)
        leftDoorSide.eulerAngles = SCNVector3.init(0, -90.0.degreesToRadians, 0)
        
        let rightDoorSide = createBox(isDoor: true)
        rightDoorSide.position = SCNVector3.init((length / 2 - width) - (doorLength / 2), 0, (length / 2) - width)
        rightDoorSide.eulerAngles = SCNVector3.init(0, -90.0.degreesToRadians, 0)
        
        //Create Light
        
        let light = SCNLight()
        light.type = .probe
        light.spotInnerAngle = 70
        light.spotOuterAngle = 120
        light.zNear = 0.00001
        light.zFar = 5
        light.castsShadow = true
        light.shadowRadius = 200
        light.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0)
        light.shadowMode = .deferred
        let constraint = SCNLookAtConstraint(target: bottomWall)
        constraint.isGimbalLockEnabled = true
        
        let lightNode = SCNNode()
        lightNode.light = light
        lightNode.position = SCNVector3.init(0, 0.4, 0)
        lightNode.constraints = [constraint]
        node.addChildNode(lightNode)
        
        //Adding Nodes to Main Node
        node.addChildNode(leftWall)
        node.addChildNode(rightWall)
        node.addChildNode(topWall)
        node.addChildNode(bottomWall)
        node.addChildNode(backWall)
        node.addChildNode(leftDoorSide)
        node.addChildNode(rightDoorSide)
        
        //letfwall
        let quadro1 = SCNScene(named: "art.scnassets/quadro1.dae")!
        if let quadro1Node = quadro1.rootNode.childNode(withName: "quadro1", recursively: true) {
            quadro1Node.position = SCNVector3((-length / 2) + 0.20 + width, 0, 0)
            quadro1Node.name = "quadro 1"
            sceneView.scene.rootNode.addChildNode(quadro1Node)
            sceneView.autoenablesDefaultLighting = true
            //            let tap = UITapGestureRecognizer(target: self, action: #selector(touchQuadro1(rec:)))
            //            sceneView.addGestureRecognizer(tap)
            
        }
        
        let quadro2 = SCNScene(named: "art.scnassets/quadro1.dae")!
        if let quadro2Node = quadro2.rootNode.childNode(withName: "quadro1", recursively: true) {
            quadro2Node.position = SCNVector3((length / 2) - 0.20 - width, 0, 0)
            quadro2Node.name = "quadro 2"
            sceneView.scene.rootNode.addChildNode(quadro2Node)
            sceneView.autoenablesDefaultLighting = true
            
            //            let tap = UITapGestureRecognizer(target: self, action: #selector(touchQuadro2(rec:)))
            //            sceneView.addGestureRecognizer(tap)
        }
        
        //backwall
        let quadro3 = SCNScene(named: "art.scnassets/quadro1.dae")!
        if let quadro3Node = quadro3.rootNode.childNode(withName: "quadro1", recursively: true) {
            quadro3Node.name = "quadro 3"
            quadro3Node.position = SCNVector3(0, 0, (-length / 2) + width + 1.5)
            
            sceneView.scene.rootNode.addChildNode(quadro3Node)
            sceneView.autoenablesDefaultLighting = true
            
            quadro3Node.runAction(SCNAction.repeatForever(SCNAction.rotateBy(x: 0, y: CGFloat(Float.pi * 2), z: 0, duration: 10)))
            
            //            let tap = UITapGestureRecognizer(target: self, action: #selector(touchQuadro3(rec:)))
            //            sceneView.addGestureRecognizer(tap)
        }
        self.sceneView.scene.rootNode.addChildNode(node)
    }
    
    
    func createTextNode(string: String, vector: SCNVector3) -> SCNNode {
        let text = SCNText(string: string, extrusionDepth: 0.1)
        text.font = UIFont.systemFont(ofSize: 1.0)
        text.flatness = 0.01
        text.firstMaterial?.diffuse.contents = UIColor.black
        
        let textNode = SCNNode(geometry: text)
        
        //let fontSize = Float(0.04)
        textNode.scale = vector
        
        return textNode
    }
        
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard let touchLocation = touches.first?.location(in: sceneView),
            let hitNode = sceneView?.hitTest(touchLocation, options: nil).first?.node,
            let nodeName = hitNode.name
            
            else {
                //No Node Has Been Tapped
                return
        }
        
        if(nodeName == "quadro 1" && !isQuadro1SubtitleDisplayed){
            let vector = SCNVector3(Float(0.04), Float(0.04), Float(0.04))
            let textNode = createTextNode(string: "quadro 1",vector: vector)
            textNode.runAction(SCNAction.rotateBy(x: 0, y: CGFloat(Float.pi / 2), z: 0, duration: 2))
            
            sceneView.scene.rootNode.addChildNode(textNode)
            textNode.position = SCNVector3((-length / 2) + 0.20 + width, 0, 0)
            print("Quadro Esquerda")
            isQuadro1SubtitleDisplayed = true;
        }
        else if(nodeName == "quadro 2" && !isQuadro2SubtitleDisplayed){
            let vector = SCNVector3(Float(0.04), Float(0.04), Float(0.04))
            let textNode = createTextNode(string: "quadro 2",vector: vector)
            textNode.runAction(SCNAction.rotateBy(x: 0, y: CGFloat(-Float.pi / 2), z: 0, duration: 2))
            sceneView.scene.rootNode.addChildNode(textNode)
            
            textNode.position = SCNVector3((length / 2) - 0.20 - width, 0, 0)
            print("Quadro Direita")
            isQuadro2SubtitleDisplayed = true;
        }
        else if(nodeName == "quadro 3" && !isQuadro3SubtitleDisplayed){
            let vector = SCNVector3(Float(0.04), Float(0.04), Float(0.04))
            let textNode = createTextNode(string: "quadro 3", vector: vector)
            
            textNode.runAction(SCNAction.rotateBy(x: 0, y: CGFloat(Float.pi * 2), z: 0, duration: 2))
            sceneView.scene.rootNode.addChildNode(textNode)
            
            textNode.position = SCNVector3(0, 0, (-length / 2) + width + 1.5)
            print("Quadro Fundo")
            isQuadro3SubtitleDisplayed = true;
        }
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func session(_ session: ARSession, didFailWithError error: Error) {}
    
    func sessionWasInterrupted(_ session: ARSession) {}
    
    func sessionInterruptionEnded(_ session: ARSession) {}

}
